using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MFMenu : MonoBehaviour
{
    // Start is called before the first frame update

    private static MFMenu instance = null;
    public static MFMenu Instance
    {
        get { return instance; }
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }
}
