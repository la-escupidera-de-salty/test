using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PegarsePlataforma : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Player;

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject == Player)
        {
            Player.transform.parent = transform;
            print("pegarse");
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if(col.gameObject == Player)
        {
            Player.transform.parent = null;
        }
    }
}
