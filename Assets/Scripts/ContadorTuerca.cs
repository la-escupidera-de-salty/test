using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ContadorTuerca : MonoBehaviour
{
   // public Text tuerca;
    public Text Tornillos;
    public Text Tuercas;
    public Text Llaves;
    public Text Total;

    int total;
    
    // Start is called before the first frame update
    void Start()
    {
        
        //tuerca.text = "Tuercas = " + PlayerPrefs.GetString("Tuercas");
        print("Tornillo=" + PlayerPrefs.GetInt("Tornillos"));
        Tornillos.text = "" + PlayerPrefs.GetInt("Tornillos");
        Tuercas.text = "" + PlayerPrefs.GetInt("Tuercas");
        Llaves.text = "" + PlayerPrefs.GetInt("Llaves");
        total = PlayerPrefs.GetInt("Tornillos") + PlayerPrefs.GetInt("Tuercas") + PlayerPrefs.GetInt("Llaves");
        Total.text = "" + total;

    }

    // Update is called once per frame
    void Update()
    {

    }
}
