using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MFGameplay : MonoBehaviour
{
    private static MFGameplay instance = null;
    public static MFGameplay Instance
    {
        get { return instance; }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
                return;
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }
}
