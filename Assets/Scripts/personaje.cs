using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class personaje : MonoBehaviour
{

    public Text Tornillos;
    public Text Tuercas;
    public Text Llaves;
    public Text Total;



    public float speed = -10;
    public float jumpForce;
    private float moveInput;

    private Animator anima;

    private ShakeCamara shake;


    private Rigidbody2D rb;

    public bool facingRight = true;

   
    public static int tornillos ;
    public static int tuercas;
    public static int llaves;
    int tornillo;
    int tuerca;
    int llave;
    int total;
    public int cuenta;
    


    // Start is called before the first frame update
    void Start()
    {
        shake = GameObject.FindGameObjectWithTag("ScreenShake").GetComponent<ShakeCamara>();
        rb = GetComponent<Rigidbody2D>();
        anima = GetComponent<Animator>();
        Tornillos.text = "0";
        Tuercas.text = "0";
        print(""+tuercas);
       
    }

    // Update is called once per frame
    void Update()
    {
        moveInput = Input.GetAxis("Horizontal");
        rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);

        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        {
            GetComponent<Animator>().SetBool("isRunning", true);
        } else
        {
            GetComponent<Animator>().SetBool("isRunning", false);
        }
        
        
        
        //anima.SetBool("caminar", true);


       /*  if (Input.GetAxis("Horizontal"))
         {
        Animation.Play("caminar");
         } */

        if (facingRight == false && moveInput > 0)
        {
            Flip();
        } else if (facingRight ==true && moveInput < 0)
        {
            Flip();
        }


        total = tornillo + tuerca + llave;
        Total.text = total + "";
    }



    void Flip()
    {
        facingRight = !facingRight;
        Vector3 Scaler = transform.localScale;
        Scaler.x *= -1;
        transform.localScale = Scaler;
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag == "picos")
        {
            shake.CamShake();
            Invoke("Muere", 0.05f);
            print("auch");
        } 
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Tuerca")
        {
            tuerca ++;
            Tuercas.text =  tuerca + "";
            Destroy(collision.gameObject);
        }
        if (collision.tag == "Tornillo")
        {
            tornillo ++;
            Tornillos.text = tornillo + "";
            Destroy(collision.gameObject);

        }

        if (collision.tag == "Llave")
        {
            llave ++;
            Llaves.text = llave + "";
            Destroy(collision.gameObject);
        }

        if (collision.tag == "Tanque")
        {
            
            tornillos = tornillos + tornillo;
            tuercas = tuercas + tuerca;
            llaves = llaves + llave;
            PlayerPrefs.SetInt("Tornillos", tornillos);
            PlayerPrefs.SetInt("Tuercas", tuercas);
            PlayerPrefs.SetInt("Llaves", llaves);
            Scene scene = SceneManager.GetActiveScene();
            int escena = scene.buildIndex;
            SceneManager.LoadScene((escena + 1));
            print("Tor=" + tornillos);
            print("Tuer=" + tuercas);
            print("Llaves" + llaves);
        }
    }

 
    public void Muere()
    {
        
        Scene scene = SceneManager.GetActiveScene();
       SceneManager.LoadScene(scene.name);

    }


}
