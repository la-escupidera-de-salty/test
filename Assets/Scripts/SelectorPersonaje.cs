using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SelectorPersonaje : MonoBehaviour
{
    public personaje personajeSeleccion = personaje.Personaje1;

    public Transform Flecha;

    public GameObject ObjetoPersonaje1;
    public GameObject ObjetoPersonaje2;
    public GameObject ObjetoPersonaje3;

    public Sprite Personaje1;
    public Sprite Personaje1Id;

    public Sprite Personaje2;
    public Sprite Personaje2Id;

    public Sprite Personaje3;
    public Sprite Personaje3Id;

    public Sprite Austronauta;

    int seleccion = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }   

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            seleccion++;
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            seleccion--;
        }

        if (seleccion > 3)
        {
            seleccion = 1;
        }
        else if (seleccion < 1)
        {
            seleccion = 3;
        }

        if (seleccion == 1)
        {
            personajeSeleccion = personaje.Personaje1;
            Flecha.position = new Vector2(ObjetoPersonaje1.GetComponent<Transform>().position.x, Flecha.position.y);
        }
        if (seleccion == 2)
        {
            personajeSeleccion = personaje.Personaje2;
            Flecha.position = new Vector2(ObjetoPersonaje2.GetComponent<Transform>().position.x, Flecha.position.y);
        }
        if (seleccion == 3)
        {
            personajeSeleccion = personaje.Personaje3;
            Flecha.position = new Vector2(ObjetoPersonaje3.GetComponent<Transform>().position.x, Flecha.position.y);
        }

        if (personajeSeleccion == personaje.Personaje1)
        {
            ObjetoPersonaje1.GetComponent<SpriteRenderer>().sprite = Personaje1;
            if (Input.GetKey(KeyCode.Space))
            {
                SceneManager.LoadScene(1);
                PlayerPrefs.SetString("Personaje", "Personaje1");

            }
        }
        else
        {
            //ObjetoPersonaje1.GetComponent<SpriteRenderer>().sprite = Personaje1Id;
        }


        if (personajeSeleccion == personaje.Personaje2)
        {
            ObjetoPersonaje1.GetComponent<SpriteRenderer>().sprite = Personaje2;
            if (Input.GetKey(KeyCode.Space))
            {
                SceneManager.LoadScene(1);
                PlayerPrefs.SetString("Personaje", "Personaje2");

            }
        }
        else
        {
            //ObjetoPersonaje2.GetComponent<SpriteRenderer>().sprite = Personaje2Id;
        }



        if (personajeSeleccion == personaje.Personaje3)
        {
            ObjetoPersonaje1.GetComponent<SpriteRenderer>().sprite = Personaje3;
            if (Input.GetKey(KeyCode.Space))
            {
                SceneManager.LoadScene(1);
                PlayerPrefs.SetString("Personaje", "Personaje3");

            }
        }
        else
        {
            //ObjetoPersonaje3.GetComponent<SpriteRenderer>().sprite = Personaje3Id;
        }
    }

    public enum personaje
    {
        Personaje1,
        Personaje2,
        Personaje3
    }
}
