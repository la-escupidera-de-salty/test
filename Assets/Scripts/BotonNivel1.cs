using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BotonNivel1 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void PasarInicio()
    {
        SceneManager.LoadScene(0);
    }

    public void PasarInstrucciones()
    {
        SceneManager.LoadScene(1);
    }
    public void PasarSeleccion()
    {
        SceneManager.LoadScene(2);
    }

    public void PasarNivel1()
    {
        SceneManager.LoadScene(3);
    }

    public void Pasate()
    {
        Scene scene = SceneManager.GetActiveScene();
        int escena = scene.buildIndex;
        SceneManager.LoadScene((escena + 1));
    }


}
